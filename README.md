# esp32-toolkit-lib

sets of common features for building esp32 firmware

# Usage

## Workspace setup

To avoid versioning boilerplate files, the PlatformIO project is not included in the repository.
It is generated instead at CI time and made available as an artifact.

Retrieve the previous workspace artifact and unzip it to your drive.

## Firmware build

Once done, following steps must be done:

- customize wifi settings in `lib/esp32-toolkit-lib/firmare-base.cpp`. 

Recommendation is to use an external file such as `Dont-Commit-Me.h` to store credentials placed outside of project or added to `.gitignore`. 
See comments in source file.

- copy firmware source file `lib/esp32-toolkit-lib/examples/firmware-base.cpp` to `src/main.cpp`.  

Until a simpler approach is found it enables using a custom firmware file, by changing source path to custom location.

- from project root, project is built using this command :

`pio run -c lib/esp32-toolkit-lib/.config/platformio.ini`. 

Note#1: that a custom configuration `platformio.ini` is used instead of the generic one at project's root.

Note#2: to avoid typing 2 commands each time and forgetting to update main file, you can mix 2 previous steps in one: `cp lib/esp32-toolkit-lib/examples/firmware-base.cpp src/main.cpp && pio run -c lib/esp32-toolkit-lib/.config/platformio.ini`

- if everything went well a firmware binary file should be generated in `.pio/build/esp32doit-devkit-v1/firmware.bin` which can then be uploaded to board device.

## Filesystem image